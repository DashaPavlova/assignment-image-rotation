#include <stdio.h>
#include "bmp.h"
#include "util.h"

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n");
}

int main(int argc, char **argv) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n");
    if (argc > 2) err("Too many arguments \n");


    struct image img = {0};

    print_rs_mes(bmp_load(argv[1], &img));
    img = rotate(img);
    print_ws_mes(bmp_save(argv[1], &img));

    return 0;
}