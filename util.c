#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "util.h"
#include "image.h"


_Noreturn void err(const char *msg, ...) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);
    va_end (args);
    exit(1);
}

int padding(uint64_t const width) {
    return ((4 - (width * sizeof(struct pixel)) % 4) % 4);
}