#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "image.h"

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_ERROR,
    WRONG_FILENAME,
    FILE_NOT_FOUND,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};


enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);

enum read_status bmp_load(char const *filename, struct image *img);

enum write_status bmp_save(char const *filename, struct image const *img);

void print_ws_mes(enum write_status ws);

void print_rs_mes(enum read_status rs);

#endif