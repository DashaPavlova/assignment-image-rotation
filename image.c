#include <stdlib.h>
#include <stdio.h>
#include "image.h"
#include "image_rotate.h"

struct image rotate(struct image const source) {
    struct image rot_img = (struct image) {
            .width = source.height,
            .height = source.width,
            .data = (struct pixel *) malloc(sizeof(struct pixel) * source.width * source.height)
    };

//    for (size_t i = 0; i < height; i++)
//        for (size_t j = 0; j < width; j++)
//            *(rot_img.data + j * height + (height - 1 - i)) = *(source.data + i * width + j);

    rotated(rot_img, source);

    printf("Image was rotated\n");

    return rot_img;
}