#ifndef _IMAGE_ROTATE_
#define _IMAGE_ROTATE_

void rotated(struct image rot_img, struct image const source) {
    for (uint64_t i = 0; i < rot_img.width; i++)
        for (uint64_t j = 0; j < rot_img.height; j++)
            *(rot_img.data + j * rot_img.width + (rot_img.width - 1 - i)) = *(source.data + i * source.width + j);
}

#endif