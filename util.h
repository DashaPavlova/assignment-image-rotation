#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdint.h>

_Noreturn void err(const char *msg, ...);

int padding(uint64_t const width);

#endif