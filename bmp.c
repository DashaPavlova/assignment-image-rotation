#include "bmp.h"
#include "util.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool read_header(FILE *f, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

//static bool read_header_from_file(const char *filename, struct bmp_header *header) {
//    if (!filename) return false;
//
//    FILE *f = fopen(filename, "rb");
//    if (!f) return false;
//
//    if (read_header(f, header)) {
//        fclose(f);
//        return true;
//    }
//
//    fclose(f);
//    return false;
//}

struct bmp_header create_rot_bh(struct image const *img) {
    return (struct bmp_header) {
            .bfType = 0x4d42,
            .bfileSize = sizeof(struct bmp_header) +
                         (sizeof(struct pixel) * img->width + padding(img->width)) * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biHeight = img->height,
            .biWidth = img->width,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (sizeof(struct pixel) * img->width + padding(img->width)) * img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum read_status from_bmp(FILE *file, struct image *img) {
    struct bmp_header bh = {0};

    if (!read_header(file, &bh)) return WRONG_FILENAME;

    img->width = bh.biWidth;
    img->height = bh.biHeight;
    img->data = (struct pixel *) malloc((img->width * sizeof(struct pixel) + padding(img->width)) * img->height);

    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, file) < 1) {
            free(img->data);
            return READ_ERROR;
        }
        fseek(file, padding(img->height), SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header const rot_bh = create_rot_bh(img);
    if (fwrite(&rot_bh, sizeof(struct bmp_header), 1, out) < 1) {
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i < img->width; i++) {
        if (fwrite(img->data + i * img->height, sizeof(struct pixel), img->height, out) < 1)
            return WRITE_ERROR;
        if (fwrite(img->data, 1, padding(img->width), out) != padding(img->width))
            return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum read_status bmp_load(char const *filename, struct image *img) {
    if (!filename) return WRONG_FILENAME;

    FILE *file = fopen(filename, "rb");
    if (!file) return FILE_NOT_FOUND;

    enum read_status rs = from_bmp(file, img);

    fclose(file);

    return rs;
}

enum write_status bmp_save(char const *filename, struct image const *img) {
    FILE *out = fopen(filename, "wb");

    if (!out) {
        fclose(out);
        return WRITE_ERROR;
    }

    enum write_status ws = to_bmp(out, img);
    fclose(out);

    return ws;
}

static const char *const rs_mes[] = {
        [READ_OK] = "File was read\n",
        [WRONG_FILENAME] = "Error reading file\nWrong filename",
        [FILE_NOT_FOUND] = "Error reading file\nFile not found\n",
        [READ_ERROR] = "Error reading file\n"
};

static const char *const ws_mes[] = {
        [WRITE_OK] = "File was written\n",
        [WRITE_ERROR] = "Error writing to file\n"
};

void print_rs_mes(enum read_status rs) {
    if (rs != READ_OK) {
        fprintf(stderr, "%s", rs_mes[rs]);
        exit(-1);
    } else (printf("%s", rs_mes[rs]));
}

void print_ws_mes(enum write_status ws) {
    if (ws != WRITE_OK) {
        fprintf(stderr, "%s", ws_mes[ws]);
        exit(-1);
    } else (printf("%s", ws_mes[ws]));
}